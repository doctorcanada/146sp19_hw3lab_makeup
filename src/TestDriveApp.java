/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bcanada
 */
public class TestDriveApp {
    
    public static void main( String[] args )
    {
        Truck truck = new Truck( 0.0, 90 );
        Droid droid = new Droid();
        
        System.out.println(
            "Truck direction is currently " + truck.getDirection() + " degrees" );
        System.out.println(
            "Truck speed is currently " + truck.getSpeed() + " mph" );
        
        truck.go();
        truck.turnLeft();
        
        System.out.println(
            "Truck direction is currently " + truck.getDirection() + " degrees" );
        System.out.println(
            "Truck speed is currently " + truck.getSpeed() + " mph" );
        
        truck.turnRight();
        truck.stop();
        
        System.out.println(
            "Truck direction is currently " + truck.getDirection() + " degrees" );
        System.out.println(
            "Truck speed is currently " + truck.getSpeed() + " mph" );
        
        droid.go();
        droid.stop();
        
    } // end method main
    
} // end class TestDriveApp
